lqa (20230104~git9be8db8ab65c-2) unstable; urgency=medium

  * QA upload.
  * debian/control:
    - Add Extended Description.
    - Add Rules-Requires-Root field.
    - Bump Standards-Version to "4.7.0".
    - Improve short description.
  * debian/copyright:
    - Add Andrej Shadura to packaging copyright.
    - Update packaging copyright information.
  * debian/upstream/metadata: Add upstream metadata information.

 -- Rafael Schmetcka da Silva <rafael.schmetcka@gmail.com>  Sun, 16 Jun 2024 04:23:59 -0300

lqa (20230104~git9be8db8ab65c-1) unstable; urgency=medium

  * New upstream snapshot:
    - Requests based transport for server proxy.
    - Make `lqa wait` also wait for job status Scheduled.
    - Update the examples to use the same example as LAVA itself.
    - Fix jobs status methods for new LAVA API.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 04 Jan 2023 15:33:36 +0100

lqa (20210324~git9fff6ab09969-2) unstable; urgency=medium

  * Add missing build dependency on python3-docutils.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 19 Jan 2022 16:53:40 +0100

lqa (20210324~git9fff6ab09969-1) unstable; urgency=medium

  * Orphan the package.
  * Build and install the manpage.
  * Use dh-sequence-python3.
  * Wrap and sort.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 19 Jan 2022 15:59:20 +0100

lqa (20191129~git41a4806-2) unstable; urgency=medium

  * Don’t depend on python3-all-dev (Closes: #910099).
  * Ship README (Closes: #850245).

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 21 Dec 2019 11:29:57 +0100

lqa (20191129~git41a4806-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field.

  [ Andrej Shadura ]
  * New upstream snapshot.
  * Add a watch file.
  * Update gbp.conf.
  * Drop Python 2 support (Closes: #936968).
  * Bump debhelper from old 9 to 12.
  * d/control: Add .git to Vcs-Git.
  * Bump Standards-Version to 4.4.1.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 29 Nov 2019 18:43:33 +0100

lqa (20180702.0-1) unstable; urgency=medium

  * New upstream release.
  * Use dh-python and pybuild.
  * Ship code for both Python 2 and Python 3.
  * Add myself to uploaders.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 02 Jul 2018 17:36:12 +0200

lqa (20180227.0-1) unstable; urgency=medium

  * New upstream release.

 -- Héctor Orón Martínez <zumbi@debian.org>  Tue, 27 Feb 2018 15:00:27 +0100

lqa (20161215.0-2) unstable; urgency=medium

  * Point Vcs-* fields to collab-maint repos.
  * Add gbp configuration file

 -- Héctor Orón Martínez <zumbi@debian.org>  Sun, 18 Dec 2016 01:20:15 +0100

lqa (20161215.0-1) unstable; urgency=medium

  * New upstream release
  * Change source format to quilt.

 -- Héctor Orón Martínez <zumbi@debian.org>  Sat, 17 Dec 2016 21:31:21 +0100

lqa (20160621.0) unstable; urgency=medium

  * Initial release (Closes: #789041)

 -- Héctor Orón Martínez <zumbi@debian.org>  Mon, 19 Sep 2016 21:49:33 +0200
