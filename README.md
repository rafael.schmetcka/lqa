# LAVA QA Tool

Command line tool for LAVA quality assurance tasks.

## Install software from upstream code

```sh
git clone https://gitlab.collabora.com/collabora/lqa.git
cd lqa/
sudo python setup.py install
```

## Usage example

LAVA is just an automated framework to push in jobs, then those jobs run
the useful test case definitions.

The `examples/lqa.yaml` file is provided as an example that the developer 
must update before lqa usage and place it into the directory `$HOME/.config/`.

Extra examples on jobs are also provided in the `examples` directory.

Now for the real usage, tests can be run in a lot against certain image:

```sh
lqa --log-file lqa-sin-validator.log \
    submit \
    -g examples/profiles.yaml \
    --all-profiles \
    -t image_date:150305-004801 \
    examples/templates/*.json
```

The previous example will run all jobs from `examples/templates/*.json`,
using the configuration file `$HOME/.config/lqa.yaml`, for all profiles
configured in the file `examples/profiles.yaml`. The image to be used will
be the one with timestamp `150305-004801` and actions will be logged at 
`lqa-sin-validator.log`.

If the `lqa.yaml` configuration file is located in another directory,
the `--config` or `-c` options can be used like:

```sh
lqa --config examples/lqa.yaml \
    --log-file lqa-sin-validator.log \
    submit \
    -g examples/profiles.yaml \
    --all-profiles \
    -t image_date:150305-004801 \
    examples/templates/*.json
```

The previous example will run all jobs in `examples/templates/*.json`, using 
the configuration file `examples/lqa.yaml`, for all profiles configured 
in the file `examples/profiles.yaml`. The image to be used will be the one 
with timestamp `150305-004801` and actions will be logged at `lqa-sin-validator.log`.
