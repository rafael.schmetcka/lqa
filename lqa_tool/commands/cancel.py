###################################################################################
# LAVA QA tool
# Copyright (C) 2015, 2016 Collabora Ltd.
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

try:
    from xmlrpc.client import Fault
except ImportError:
    from xmlrpclib import Fault
from lqa_api.exit_codes import APPLICATION_ERROR
from lqa_tool.settings import lqa_logger
from lqa_tool.commands import Command

class CancelCmd(Command):

    def __init__(self, args):
        Command.__init__(self, args)

    def run(self):
        """Cancel job id"""
        for job_id in self.job_ids:
            cancel(job_id, self.server)

def cancel(job_id, conn):
    """
    This method can be used by other commands requiring to cancel jobs.
    """
    try:
        conn.cancel_job(job_id)
        lqa_logger.info("Job {} canceled".format(job_id))
    except Fault as e:
        lqa_logger.error("lqa cancel {}: {}".format(job_id, e))
    except EnvironmentError as e:
        lqa_logger.error(e)
        exit(APPLICATION_ERROR)
