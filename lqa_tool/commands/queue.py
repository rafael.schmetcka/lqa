###################################################################################
# LAVA QA tool
# Copyright (C) 2015, 2016 Collabora Ltd.
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from __future__ import print_function
from lqa_tool.commands import QueueCommand

class QueueCmd(QueueCommand):

    def __init__(self, args):
        QueueCommand.__init__(self, args)

    def run(self):
        QueueCommand.run(self, _showqueue)

def _showqueue(job, _, job_data, args):
    time_fmt = ''
    if args.show_time:
        status = job_data[2]
        if status == 'submitted':
            time_fmt = "(submitted at: {})".format(job.submit_time)
        elif status == 'running':
            time_fmt = "(running since: {})".format(job.start_time)
    # Print queued job
    print(job, time_fmt)
